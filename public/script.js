const sleep = (ms) => new Promise((r) => setTimeout(r, ms));
const morseCodeLetters = {
	0: "-----",
	1: ".----",
	2: "..---",
	3: "...--",
	4: "....-",
	5: ".....",
	6: "-....",
	7: "--...",
	8: "---..",
	9: "----.",
	a: ".-",
	b: "-...",
	c: "-.-.",
	d: "-..",
	e: ".",
	f: "..-.",
	g: "--.",
	h: "....",
	i: "..",
	j: ".---",
	k: "-.-",
	l: ".-..",
	m: "--",
	n: "-.",
	o: "---",
	p: ".--.",
	q: "--.-",
	r: ".-.",
	s: "...",
	t: "-",
	u: "..-",
	v: "...-",
	w: ".--",
	x: "-..-",
	y: "-.--",
	z: "--..",
	".": ".-.-.-",
	",": "--..--",
	"?": "..--..",
	"!": "-.-.--",
	"-": "-....-",
	"/": "-..-.",
	"@": ".--.-.",
	"(": "-.--.",
	")": "-.--.-",
};
const wordBank = [
	"shell",
	"halls",
	"slick",
	"trick",
	"boxes",
	"leaks",
	"strobe",
	"bistro",
	"flick",
	"bombs",
	"break",
	"brick",
	"steak",
	"sting",
	"vector",
	"beats",
];

let ac = new AbortController();

let timeUnitMs = 500;

function turnOn() {
	document.getElementById("blink-light").classList.add("on");
}
function turnOff() {
	document.getElementById("blink-light").classList.remove("on");
}

async function dash(signal) {
	if (!signal.aborted) {
		turnOn();
	}
	if (!signal.aborted) {
		await sleep(timeUnitMs * 3);
	}
	if (!signal.aborted) {
		turnOff();
	}
	if (!signal.aborted) {
		await sleep(timeUnitMs);
	}
}

async function dot(signal) {
	if (!signal.aborted) {
		turnOn();
	}
	if (!signal.aborted) {
		await sleep(timeUnitMs);
	}
	if (!signal.aborted) {
		turnOff();
	}
	if (!signal.aborted) {
		await sleep(timeUnitMs);
	}
}

async function letterPause(signal) {
	if (!signal.aborted) {
		await sleep(timeUnitMs * 3);
	}
}

async function wordPause(signal) {
	if (!signal.aborted) {
		await sleep(timeUnitMs * 7);
	}
}

async function letter(l, signal) {
	const letr = l + "";
	const morse = morseCodeLetters[letr].split("");
	for (let i = 0; i < morse.length; i++) {
		if (!signal.aborted) {
			if (morse[i] == "-") {
				await dash(signal);
			} else if (morse[i] == ".") {
				await dot(signal);
			}
		} else {
			return;
		}
	}
	await letterPause(signal);
}

async function word(word, signal) {
	const letters = word.split("");
	console.log(letters);
	for (let i = 0; i < letters.length; i++) {
		await letter(letters[i], signal);
	}
	await wordPause(signal);
}

async function playRandomWord() {
	cancel();
	randomWord = wordBank[Math.floor(Math.random() * wordBank.length)];
	document.getElementById("textInput").oninput = (event) =>
		checkInput(event, randomWord);
	const signal = ac.signal;
	while (!signal.aborted) {
		await word(randomWord, signal);
	}
}

async function checkInput(event, word) {
	if (event.target.value.toLowerCase() === word) {
		document.getElementById("sucessInfo").innerHTML =
			"Congratulations ! The word was " + word;
		await cancel();
		event.target.oninput = () => {};
	}
}

async function cancel() {
	ac.abort();
	turnOff();
	ac = new AbortController();
}

document.getElementById("timeUnitRange").oninput = (event) =>
	timeUnitChange(event);

function timeUnitChange(a) {
	timeUnitMs = a.target.value;
	document.getElementById("timeUnitText").innerHTML =
		"Time unit: " + timeUnitMs;
}
